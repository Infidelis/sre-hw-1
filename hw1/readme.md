- [x] Установите Oncall. 

На порту 8080, семинарский сервер

- [x] Создать тестовых пользователей, больше 3.

См. скрипт **create_users.sh**



- [x] Завести команды 2-3 команды, больше 1

Вручную созданы:

![](./img/browse_teams.png)
![](./img/team-info.png)


- [x] Заполнить расписание дежурств на 2 месяца вперед

Заполнение через скрипт **schedule_team_duty_roster.py**.

Перед этим нужно установить зависимости из `requirements.txt`, `python 3.9`.

![](./img/api_calendar.png)


Пример реализации варианта -- **Пересменка каждые 4 дня в 10 часов утра**:

![](./img/calendar_november.png)

![](./img/calendar_december.png)

![](./img/calendar_january.png)