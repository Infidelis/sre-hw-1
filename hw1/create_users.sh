#!/bin/bash

HOST=http://localhost:8080

ADMIN_USER=admin
ME=v.mamedov
TEST_USER_1=test-1
TEST_USER_2=test-2

for USER_LOGIN in $ME $ADMIN_USER $TEST_USER_1 $TEST_USER_2
do
    # delete first
    echo "delete user if exists: $USER_LOGIN"
    curl --location --request DELETE "$HOST/api/v0/users/$USER_LOGIN"

    # create
    echo "create user: $USER_LOGIN"
    curl --location --request POST "$HOST/api/v0/users" \
        --header 'Expect:' \
        --header 'Content-Type: application/json; charset=utf-8' \
        --data-raw "{\"name\": \"$USER_LOGIN\", \"active\": 1, \"time_zone\": \"Europe/Moscow\"}"
done

# do not work
#echo "add admin god privileges"
#curl --location --request PUT "$HOST/api/v0/users/$ADMIN_USER" \
#    --header 'Expect:' \
#    --header 'Content-Type: application/json; charset=utf-8' \
#    --data-raw "{\"name\": \"$ADMIN_USER\", \"active\": 1, \"time_zone\": \"Europe/Moscow\", \"god\": 1}"

GROUP_1=admin-team-1
GROUP_2=admin-team-2
GROUP_3=admin-team-3
#
#for GROUP in $GROUP_1 $GROUP_2 $GROUP_3
#do
#    # create
#    echo "create group: $GROUP"
#    # The user who creates the team is automatically added as a team admin. Because of this, this endpoint cannot be called using an API key, otherwise a team would have no admins, making many team operations impossible.
#    curl --location --request PUT "$HOST/api/v0/teams/$GROUP" \
#        --header 'Expect:' \
#        --header 'Content-Type: application/json; charset=utf-8' \
#        --data-raw "{\"name\": \"$GROUP\", \"scheduling_timezone\": \"Europe/Moscow\"}"
#
#done