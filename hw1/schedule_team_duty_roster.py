from dataclasses import dataclass
from datetime import datetime, timedelta
import itertools
from typing import Optional, Literal

import typer
import httpx

app = typer.Typer()


@dataclass
class OncallApi:
    oncall_base_url: str

    def __post_init__(self):
        self._client = httpx.Client(base_url=self.oncall_base_url)

    @staticmethod
    def _datetime_to_unix(dttm: datetime) -> int:
        return int(dttm.timestamp())

    def delete_scheduled_events(self, start_dttm: datetime, team_name: str):
        start_dttm_unix = self._datetime_to_unix(start_dttm)

        primary_events = self._client.get(
            f"api/v0/events",
            params={
                "team": team_name,
                "start__ge": start_dttm_unix,
                "role": "primary"
            },
        ).json()
        secondary_events = self._client.get(
            f"api/v0/events",
            params={
                "team": team_name,
                "start__ge": start_dttm_unix,
                "role": "secondary",
            },
        ).json()

        for event in primary_events + secondary_events:
            self._client.delete(f"api/v0/events/{event['id']}")

    def get_team_roster_members(self, team_name: str, roster_name: str, in_rotation: Optional[bool] = None) -> list[
        dict]:
        rost_members = self._client.get(f"api/v0/teams/{team_name}").json()["rosters"][
            roster_name
        ]["users"]
        if in_rotation is not None:
            rost_members = [x for x in rost_members if x["in_rotation"] == in_rotation]
        return rost_members

    def schedule_duty_roster(
            self,
            team_name: str,
            member_name: str,
            start_dttm: datetime,
            end_dttm: datetime,
            role: Literal['primary', 'secondary'] = 'primary'
    ) -> None:
        self._client.post(
            f"api/v0/events",
            json={
                "user": member_name,
                "team": team_name,
                "role": role,
                "start": self._datetime_to_unix(start_dttm),
                "end": self._datetime_to_unix(end_dttm),
            },
        )


@app.command()
def schedule_team_duty_roster(
        start_datetime: datetime = typer.Option(default="2022-11-20"),
        days_to_book: int = typer.Option(default=60),
        days_to_shift: int = typer.Option(default=4),
        shift_hour: int = typer.Option(default=10),
        team_name: str = typer.Option(default='admin-team-1'),
        roster_name: str = typer.Option(default='on_duty'),
        oncall_base_url: str = typer.Option(default='http://localhost:8080')
):
    oncall_api = OncallApi(
        oncall_base_url=oncall_base_url,
    )
    oncall_api.delete_scheduled_events(start_dttm=start_datetime, team_name=team_name)
    in_rotation_roster_members = oncall_api.get_team_roster_members(
        team_name=team_name,
        roster_name=roster_name,
        in_rotation=True
    )

    members = itertools.cycle(in_rotation_roster_members)
    cur_datetime = start_datetime.replace(hour=shift_hour)

    end_datetime = start_datetime + timedelta(days=days_to_book)
    while cur_datetime < end_datetime:
        cur_primary_shift_member = next(members)
        cur_secondary_shift_member = next(members)

        cur_datetime_start = cur_datetime
        shift_end_datetime = cur_datetime + timedelta(days=days_to_shift)

        while cur_datetime_start < shift_end_datetime:
            cur_datetime_end = cur_datetime_start + timedelta(days=1)
            _default_params = dict(
                team_name=team_name,
                start_dttm=cur_datetime_start,
                end_dttm=cur_datetime_end,
            )
            oncall_api.schedule_duty_roster(
                member_name=cur_primary_shift_member["name"],
                **_default_params
            )
            oncall_api.schedule_duty_roster(
                member_name=cur_secondary_shift_member["name"],
                role='secondary',
                **_default_params
            )

            (
                cur_primary_shift_member,
                cur_secondary_shift_member,
                cur_datetime_start
            ) = (
                cur_secondary_shift_member,
                cur_primary_shift_member,
                cur_datetime_end
            )

        cur_datetime = shift_end_datetime


if __name__ == "__main__":
    app()
